from utils.data_load_utils import QADataSet
from utils.data_load_utils import load_data


class SquadDataSet(QADataSet):

    def __init__(self, data_path, max_data_count=None,
                 max_context_seq_length=None,
                 max_question_seq_length=None,
                 max_target_seq_length=None):
        super(SquadDataSet, self).__init__()
        load_data(self.data, data_path=data_path,
                  max_data_count=max_data_count,
                  max_context_seq_length=max_context_seq_length,
                  max_question_seq_length=max_question_seq_length,
                  max_target_seq_length=max_target_seq_length)

    def load_model(self, data_path, max_data_count=None,
                   max_context_seq_length=None,
                   max_question_seq_length=None,
                   max_target_seq_length=None):
        load_data(self.data, data_path=data_path,
                  max_data_count=max_data_count,
                  max_context_seq_length=max_context_seq_length,
                  max_question_seq_length=max_question_seq_length,
                  max_target_seq_length=max_target_seq_length)


def main():
    data_set = SquadDataSet(data_path="../data/train-v1.1.json")
    print("size: ", data_set.size())

    for index in range(100):
        context, question, answer = data_set.get_data(index)
        print('#' + str(index) + ' context:', context)
        print('#' + str(index) + ' question:', question)
        print('#' + str(index) + ' answer:', answer)
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++')


if __name__ == "__main__":
    main()
