from models.seq2seq import Seq2SeqQA
from explore_dataset import SquadDataSet
import numpy as np


def main():
    random_state = 42
    output_dir_path = "../gen_models"

    np.random.seed(random_state)
    data_set = SquadDataSet(data_path="../data/train-v1.1.json")

    qa = Seq2SeqQA()
    batch_size = 64
    epochs = 10
    history = qa.fit(data_set, model_dir_path=output_dir_path,
                     epochs=epochs, batch_size=batch_size, random_state=random_state)


if __name__ == "__main__":
    main()